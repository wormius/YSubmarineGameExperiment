#ifndef EYE_H
#define EYE_H

#include <SFML/Graphics.hpp>

#include <cmath>

class Eye : public sf::Sprite
{
    public:
        Eye(sf::Vector2f);
        virtual ~Eye();
        void            update(sf::Vector2f pos,sf::Vector2f look_at);



    private:
        sf::Texture     texture;
};

#endif // EYE_H
