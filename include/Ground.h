#ifndef GROUND_H
#define GROUND_H

#include <SFML/Graphics.hpp>


class Ground :  public sf::Sprite
{
    public:
        Ground(float, float, sf::Texture &texture);
        virtual ~Ground();


    private:
};

#endif // GROUND_H
