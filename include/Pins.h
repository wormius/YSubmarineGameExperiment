#ifndef PINS_H
#define PINS_H

#include <SFML/Graphics.hpp>


class Pins : public sf::Sprite
{
    public:
        Pins(float x, float y, sf::Texture &texture);
        virtual ~Pins();

    private:

};

#endif // PINS_H
