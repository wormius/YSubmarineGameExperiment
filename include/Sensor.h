#ifndef SENSOR_H
#define SENSOR_H

#include <GoIO/GoIO_DLL_interface.h>
#include <cstdlib>
#include <iostream>

class Sensor
{
    public:
        Sensor();
        virtual ~Sensor();


        float               get_measure();
        void                set_zero();



    private:
        gtype_real64        period;

        gtype_int32 GOIO_MAX_SIZE_DEVICE_NAME;

        char deviceName[260];
        gtype_int32 vendorId;
        gtype_int32 productId;

        GOIO_SENSOR_HANDLE hDevice;

        float               sensor_zero;

        bool GetAvailableDeviceName(char *deviceName, gtype_int32 nameLength, gtype_int32 *pVendorId, gtype_int32 *pProductId);


};

#endif // SENSOR_H
