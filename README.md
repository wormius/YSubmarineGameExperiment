# YSubmarineGameExperiment

YSubmarineGameExperiment es un simple juego experimental que permite controlar un personaje a través de un dispositivo basado en la ley de inducción de Faraday, utilizando un sensor de corriente eléctrica compatible con la interfaz Go! Link de Vernier Software & Technology, para Linux.

El juego YSubmarineGameExperiment está escrito en C++ y utiliza las librerías de GoIO SDK y SFML.

![YSubmarineGameExperiment](https://gitlab.com/wormius/YSubmarineGameExperiment/raw/master/sources/ys.png  "YSubmarineGameExperiment")
***


## Requisitos previos a la compilación de YSubmarineGameExperiment

Para compilar y obtener el ejecutable, se necesita tener instalados:

- make
- g++
- SFML (libsfml-dev)
- GoIO SDK (libGoIO.so)

### Instalación de requisitos

Para instalar **make**, **g++** y **SFML**:

En Ubuntu y derivados:
~~~
sudo apt-get install make build-essential libsfml-dev
~~~

Para instalar **GoIO SDK**:

- Instalar dependencias:
~~~
sudo apt-get install aptitude
sudo aptitude install build-essential
sudo aptitude install automake
sudo aptitude install dpkg-dev
sudo aptitude install libtool
sudo aptitude install libusb-1.0-0-dev
~~~

- Descargar el repositorio: [GoIO SDK](https://github.com/VernierSoftwareTechnology/GoIO_SDK) 
- Descomprimir el paquete descargado
- Entrar en la carpeta ***src*** de GoIO SDK
- Localizar y abrir con un editor de textos el archivo ***autogen.sh***
- Remplazar la linea:

~~~
for am in automake-1.7 automake-1.8 automake-1.9  automake; do
~~~
por
~~~
for am in automake-1.7 automake-1.8 automake-1.9 automake-1.XX automake; do
~~~
donde ***automake-1.XX*** debe corresponder a la versión de automake que tienes instalada.

- En terminal, dentro de la carpeta ***src*** de GoIO SDK:
~~~
./build.sh
~~~

## Compilación y ejecución de YSubmarineGameExperiment

### Compilación
Escribir en terminal, dentro de la capeta de YSubmarineGameExperiment:
~~~
make
~~~

### Ejecución
Despues de compilar, escribir en terminal, dentro de la capeta de YSubmarineGameExperiment:
~~~
./YSubmarineGame
~~~

## Cómo jugar YSubmarineGameExperiment

### Construir un mando

**Requisitos**

- Sensor de corriente eléctrica conectado a interfaz Go! Link
- Bobina de cobre
- Imán

**Procedimiento**

El juego leerá la corriente eléctrica variante en el tiempo producida en el acto de introducir y extraer repetidamente el imán en la bobina de cobre. Ver [Ley de Faraday](http://hyperphysics.phy-astr.gsu.edu/hbasees/electric/farlaw.html) para más detalles.

La forma más simple de montar un dispositivo para controlar el juego, consiste en conectar los dos extremos de la bobina al sensor de intensidad de corriente eléctrica e inducir la corriente con el imán de forma manual.

El juego reaccionará a la velocidad con que la corriente alcance picos máximos.

### Jugar

- Montar y conectar el mando.
- Ejecutar el juego.
- Presionar la tecla **Z** para calibrar el sensor e iniciar el juego.
- Inducir corriente eléctrica en la bobina para que el submarino se eleve y evite los obstaculos.



