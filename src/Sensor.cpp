#include "Sensor.h"

Sensor::Sensor()
:period(0.010),
GOIO_MAX_SIZE_DEVICE_NAME(260)

{
    GoIO_Init();
    bool bFoundDevice = GetAvailableDeviceName(deviceName, GOIO_MAX_SIZE_DEVICE_NAME, &vendorId, &productId);

    if (!bFoundDevice){

        std::cout << "Not sensor." << std::endl;
        //std::exit(1);

    }
    else{

        hDevice = GoIO_Sensor_Open(deviceName, vendorId, productId, 0);
		if (hDevice != NULL){


            GoIO_Sensor_SetMeasurementPeriod(hDevice, 0.016, SKIP_TIMEOUT_MS_DEFAULT);//10 milliseconds measurement period.
            GoIO_Sensor_SendCmdAndGetResponse(hDevice, SKIP_CMD_ID_START_MEASUREMENTS, NULL, 0, NULL, NULL, SKIP_TIMEOUT_MS_DEFAULT);
            sensor_zero = 0;
        }

    }

}

Sensor::~Sensor()
{
    GoIO_Uninit();
}

float Sensor::get_measure(){

    float measure;

    gtype_int32 value = GoIO_Sensor_GetLatestRawMeasurement(hDevice);
    measure = GoIO_Sensor_CalibrateData(hDevice,GoIO_Sensor_ConvertToVoltage(hDevice,value));

    return measure*1000.0f - sensor_zero;

}

void Sensor::set_zero(){

    sensor_zero += this->get_measure();
}

bool Sensor::GetAvailableDeviceName(char *deviceName, gtype_int32 nameLength, gtype_int32 *pVendorId, gtype_int32 *pProductId){

    bool bFoundDevice = false;
	deviceName[0] = 0;
	int numSkips = GoIO_UpdateListOfAvailableDevices(VERNIER_DEFAULT_VENDOR_ID, SKIP_DEFAULT_PRODUCT_ID);

	if (numSkips > 0)
	{
		GoIO_GetNthAvailableDeviceName(deviceName, nameLength, VERNIER_DEFAULT_VENDOR_ID, SKIP_DEFAULT_PRODUCT_ID, 0);
		*pVendorId = VERNIER_DEFAULT_VENDOR_ID;
		*pProductId = SKIP_DEFAULT_PRODUCT_ID;
		bFoundDevice = true;
	}


	return bFoundDevice;


}
