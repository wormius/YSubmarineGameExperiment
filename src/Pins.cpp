#include "Pins.h"

Pins::Pins(float x, float y, sf::Texture &texture):
    sf::Sprite()
{
    this->setTexture(texture);
    this->setOrigin(sf::Vector2f(0,this->getLocalBounds().height));
    this->setPosition(sf::Vector2f(x,y));
}

Pins::~Pins()
{
    //dtor
}
