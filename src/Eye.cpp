#include "Eye.h"

Eye::Eye(sf::Vector2f pos_ini):
    sf::Sprite()
{
    texture.loadFromFile("sources/eye2.png");
    this->setTexture(texture);
    this->setOrigin(this->getLocalBounds().width/2, this->getLocalBounds().height/2);
    this->setPosition(pos_ini);
}

Eye::~Eye()
{
    //dtor
}

void Eye::update(sf::Vector2f pos, sf::Vector2f look_at)
{
    sf::Vector2f        dir;
    dir = look_at - pos;
    float norm = std::sqrt(std::pow(dir.x,2) + std::pow(dir.y,2));
    dir = sf::Vector2f(dir.x/norm, dir.y/norm);
    dir = sf::Vector2f(10*dir.x, 10*dir.y);
    this->setPosition(pos+ dir);
}
